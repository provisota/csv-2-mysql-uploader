package com.service.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Ilya Alterovych
 */
@SpringBootApplication
public class SpringBootCSV {

  public static void main(String[] args) {
    SpringApplication.run(SpringBootCSV.class, args);
  }
}
