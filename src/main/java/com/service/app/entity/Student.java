package com.service.app.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvDate;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.time.DateUtils;

/**
 * @author Ilya Alterovych
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@JsonInclude(Include.NON_EMPTY)
public class Student {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int studentId;

  @CsvBindByName
  private String firstName;

  @CsvBindByName
  private String lastName;

  @CsvBindByName
  private String email;

  @CsvBindByName(column = "phone number")
  private String phoneNumber;

  @CsvBindByName
  private Character gender;

  @CsvBindByName
  private String information;

  @CsvBindByName(column = "birth date")
  @CsvDate("dd.MM.yyyy")
  private Date dob;

  public Date getDob() {
    return DateUtils.truncate(dob, Calendar.DAY_OF_MONTH);
  }
}
