package com.service.app.api;

import com.service.app.entity.Student;
import com.service.app.service.StudentService;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * @author Ilya Alterovych
 */
@Controller
public class StudentsController implements StudentsApi {

  private StudentService studentService;

  @Autowired
  StudentsController(StudentService studentService) {
    this.studentService = studentService;
  }

  public String getUploadForm(Model model) {
    model.addAttribute("students", studentService.getAllStudents());
    return "uploadForm";
  }

  public String uploadFile(@RequestPart(value = "file") MultipartFile multipartFile,
      RedirectAttributes redirectAttributes) throws IOException {
    try {
      if (multipartFile == null) {
        throw new IllegalArgumentException();
      }
      studentService.uploadFile(multipartFile);
    } catch (FileNotFoundException | IllegalArgumentException e) {
      e.printStackTrace();
      redirectAttributes.addFlashAttribute("message",
          "Specified file not found! Please try again.");
      return "redirect:/students/";
    }
    redirectAttributes.addFlashAttribute("message",
        "You successfully uploaded " + multipartFile.getOriginalFilename() + "!");
    return "redirect:/students/";
  }

  @ResponseBody
  public List<Student> getStudents() {
    return studentService.getAllStudents();
  }
}