package com.service.app.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Ilya Alterovych
 */
@Controller
public class RootController {

  @GetMapping("/")
  String redirectToStudents() {
    return "redirect:/students/";
  }
}
