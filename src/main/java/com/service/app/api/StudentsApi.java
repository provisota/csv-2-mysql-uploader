package com.service.app.api;

import com.service.app.entity.Student;
import java.io.IOException;
import java.util.List;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * @author Ilya Alterovych
 */
@RequestMapping("/students")
public interface StudentsApi {

  @GetMapping("/")
  String getUploadForm(Model model);

  @PostMapping("/")
  String uploadFile(MultipartFile multipartFile,
      RedirectAttributes redirectAttributes) throws IOException;

  @GetMapping("/all")
  List<Student> getStudents();
}
