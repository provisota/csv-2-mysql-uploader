package com.service.app.service;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.service.app.dao.StudentDAO;
import com.service.app.entity.Student;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class StudentServiceImpl implements StudentService {

  private final StudentDAO studentDAO;

  @Autowired
  public StudentServiceImpl(StudentDAO studentDAO) {
    this.studentDAO = studentDAO;
  }

  public void uploadFile(MultipartFile multipartFile) throws IOException {
    File file = convertMultiPartToFile(multipartFile);

    try (Reader reader = new FileReader(file)) {
      @SuppressWarnings("unchecked")
      CsvToBean<Student> csvToBean = new CsvToBeanBuilder<Student>(reader)
          .withType(Student.class)
          .withIgnoreLeadingWhiteSpace(true)
          .build();
      List<Student> studentList = csvToBean.parse();
      studentDAO.batchStore(studentList);
    }
  }

  public List<Student> getAllStudents() {
    return studentDAO.getAllStudents();
  }

  private File convertMultiPartToFile(MultipartFile multipartFile) throws IOException {
    String originalFilename = multipartFile.getOriginalFilename();
    if (originalFilename == null) {
      throw new IllegalArgumentException();
    }
    File convFile = new File(originalFilename);
    try (FileOutputStream fos = new FileOutputStream(convFile)) {
      fos.write(multipartFile.getBytes());
    }
    return convFile;
  }
}