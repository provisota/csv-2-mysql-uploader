package com.service.app.service;

import com.service.app.entity.Student;
import java.io.IOException;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Ilya Alterovych
 */
public interface StudentService {

  void uploadFile(MultipartFile multipartFile) throws IOException;

  List<Student> getAllStudents();
}
