package com.service.app.dao;

import com.service.app.entity.Student;
import com.service.app.repository.StudentRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Ilya Alterovych
 */
@Repository
public class StudentDAO {

  private final StudentRepository studentRepository;

  @Autowired
  public StudentDAO(StudentRepository studentRepository) {
    this.studentRepository = studentRepository;
  }

  public void batchStore(List<Student> studentList) {
    studentRepository.saveAll(studentList);
  }

  public List<Student> getAllStudents() {
    return studentRepository.findAll();
  }
}
