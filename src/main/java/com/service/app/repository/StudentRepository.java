package com.service.app.repository;

import com.service.app.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Ilya Alterovych
 */
@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {

}
