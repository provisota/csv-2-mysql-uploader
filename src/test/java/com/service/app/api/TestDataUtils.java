package com.service.app.api;

import com.service.app.entity.Student;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

/**
 * @author Ilya Alterovych
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
final class TestDataUtils {

  private static int id = 1;

  static List<Student> getExpectedStudentsList() {
    List<Student> students = new ArrayList<>();
    students.add(Student.builder()
        .studentId(id++)
        .firstName("Ivan")
        .lastName("Petrovych")
        .email("ivan@gmail.com")
        .phoneNumber("85538897424")
        .gender('M')
        .information("some info")
        .dob(getDateFromString("21.06.1989"))
        .build()
    );
    students.add(Student.builder()
        .studentId(id++)
        .firstName("Petya")
        .lastName("Ivanovych")
        .email("piter@gmail.com")
        .phoneNumber("12345678")
        .gender('M')
        .information("some info")
        .dob(getDateFromString("25.09.1978"))
        .build()
    );
    students.add(Student.builder()
        .studentId(id++)
        .firstName("Alex")
        .lastName("Pupkin")
        .email("alex@gmail.com")
        .phoneNumber("996662233")
        .gender('M')
        .information("some info")
        .dob(getDateFromString("02.04.1995"))
        .build()
    );
    students.add(Student.builder()
        .studentId(id++)
        .firstName("Kate")
        .lastName("Pavlova")
        .email("d")
        .phoneNumber(null)
        .gender('F')
        .information("some info")
        .dob(getDateFromString("15.06.1982"))
        .build()
    );
    return students;
  }

  @SneakyThrows
  private static Date getDateFromString(String stringDate) {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
    return simpleDateFormat.parse(stringDate);
  }
}
