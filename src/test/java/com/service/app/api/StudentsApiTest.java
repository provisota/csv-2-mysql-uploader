package com.service.app.api;

import static com.service.app.api.TestDataUtils.getExpectedStudentsList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import com.service.app.entity.Student;
import com.service.app.service.StudentService;
import java.io.FileInputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * @author Ilya Alterovych
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WebConfig.class)
public class StudentsApiTest {

  @Autowired
  private WebApplicationContext webApplicationContext;
  @Autowired
  private StudentService studentService;

  @Test
  public void uploadFile() throws Exception {

    // create test multipart file
    String originalFileName = "students.csv";
    Path path = Paths.get(ClassLoader.getSystemResource(originalFileName).toURI());
    byte[] content = IOUtils.toByteArray(new FileInputStream(path.toFile()));
    MockMultipartFile multipartFile = new MockMultipartFile("file", originalFileName, "multipart/form-data", content);

    // check API response
    MockMvc mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    mockMvc.perform(MockMvcRequestBuilders.multipart("/students/")
        .file(multipartFile))
        .andExpect(status().is(302))
        .andExpect(flash().attribute("message", "You successfully uploaded students.csv!"))
        .andExpect(view().name("redirect:/students/"));
    List<Student> allStudentsList = studentService.getAllStudents();

    // check DB updates
    assertEquals("Students number should be equal to 4", 4, allStudentsList.size());
    assertThat("Students list is different from expected", allStudentsList, is(getExpectedStudentsList()));
    System.out.println(allStudentsList);
  }
}