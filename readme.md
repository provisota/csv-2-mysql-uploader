* **Build project and Docker container:**
`mvn dockerfile:build`
* **Push Docker container to the remote repository (optional):**
`mvn dockerfile:push` or `docker:push provisota/csv-2-mysql-uploader`
* **Run Docker container:**
`docker run -it --rm -p 8080:8080 --name csv-2-mysql-uploader provisota/csv-2-mysql-uploader`
* **Open home URL:**
http://localhost:8080