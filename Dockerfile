# build the app jar with Maven
FROM maven:3.5-jdk-8-alpine as build
COPY pom.xml pom.xml
COPY src src
RUN mvn clean install -DskipTests

# Pull Docker container base image
FROM openjdk:8-jdk-alpine
MAINTAINER "provisota@ukr.net" 
VOLUME /tmp
ARG JAR_FILE
COPY --from=build target/${JAR_FILE} app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/app.jar"]